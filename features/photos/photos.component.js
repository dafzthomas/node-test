const fetch = require('node-fetch');

async function getPhotos(keyword) {
    let photos = await fetch('https://api.unsplash.com/search/photos?query=' + keyword, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Client-ID 887b699ab7f27ce736e1ddd7999b085db14d5527f18100ffd315348a4039f829'
        }
    });

    return photos;
}

async function getRandom() {
    let photo = await fetch('https://api.unsplash.com/photos/random', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Client-ID 887b699ab7f27ce736e1ddd7999b085db14d5527f18100ffd315348a4039f829'
        }
    });

    return photo;
}

module.exports = {
    getPhotos: getPhotos,
    getRandom: getRandom
}