const express = require('express');
const app = express();
const path = require('path');

const PORT = process.env.PORT || 5000;

const router = express.Router();

const photos = require('./features/photos/photos.component');

// Test Route
router.get('/', function (req, res) {
    res.json({
        message: 'hooray! welcome to our api!'
    });
});

// Dave
router.get('/dave', function (req, res) {
    res.json({
        message: 'Alright, Dave?'
    });
});

// Get Photos
router.get('/random', function (req, res) {

    photos.getRandom().then(function (data) {
        res.json(data.body);
    });
});

// Get Photos
router.get('/photos/:keyword', function (req, res) {
    console.log(req.params);

    photos.getPhotos(req.params.keyword).then(function (data) {
        res.json(data);
    });
});

//Register
app.use('/api', router);

app.listen(PORT);